function m=Mutate_Swap(chromosome)
%chromosome=[2 4 6 8];
s=chromosome;
n1=randi(length(chromosome));
while(1)
n2=randi(length(chromosome));
if (any(n1==n2)==0)
    break;
end
end
%----------------------------
s1=s(n1);
s2=s(n2);
%----------------------------
s(n1)=s2;
s(n2)=s1;
%----------------------------
m=s;
end