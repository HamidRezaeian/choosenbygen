clc; clear; close all;
%% =============Read Graph==========
%[G,Size,Diameter,Proximity_Matrix]=Graph_Creator(0,'football.gml');%ReadFile
[G,Size,Diameter,Proximity_Matrix]=Graph_Creator(80,''); %Create Random Graph
%% =============Parameters================
answer = inputdlg('Enter Commitee Size:');
CommiteeSize=str2num(answer{:}); 
popSize=round(Size/CommiteeSize);%Number of Solutions

FitnessFunction=@Fitness;%fitness Function

pc=0.9;%Crossover Probability
nc=2*round(pc*popSize/2);%Number of Offsprings (Parnets)

pm=0.09;%Mutation Probability
nm=round(pm*popSize);%Number of Mutants

Generations=25; %Stop Condition

%% =============Config================
while (1)
answer = inputdlg('Enter Specific Config [a number between 1 to 16]:');
Config=str2num(answer{:}); 
switch Config
    case 1
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Steady';
    break;    
    case 2
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Steady';
    break;      
    case 3
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Steady';
    break;       
    case 4
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Steady';
    break;       
    case 5
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Steady';
    break;       
    case 6
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Steady';
    break;       
    case 7
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Steady';
    break;       
    case 8
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Steady';
    break;       
    case 9
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Generational';
    break;        
    case 10
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Generational';
    break;       
    case 11
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Generational';
    break;      
    case 12
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_SUS;
        SutvivorSelection='Generational';
    break;    
    case 13
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Generational';
    break;       
    case 14
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Swap;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Generational';
    break;       
    case 15
        CrossOver=@Crossover_PMX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Generational';
    break;       
    case 16
        CrossOver=@Crossover_OX;
        Mutation=@Mutate_Insert;
        ParentSelection=@Selection_Tournament;
        SutvivorSelection='Generational';
    break;              
end

end


%% ===============Solutions Building / First Parents=============
%Initialize Parent
empty_individual.Value=[];
empty_individual.Cost=[];
Solutions=repmat(empty_individual,popSize,1);
for s1=1:popSize
Solutions(s1).Value=chromosome_Creator(Size,CommiteeSize);
Solutions(s1).Cost=FitnessFunction(Proximity_Matrix,Solutions(s1).Value,CommiteeSize,Diameter);
end
%Initialize Child
Child=repmat(empty_individual,nc,1);
%Cost Evaluation
for s1=1:popSize
Solutions(s1).Cost=FitnessFunction(Proximity_Matrix,Solutions(s1).Value,CommiteeSize,Diameter);
end
% Sort Population
Costs=[Solutions.Cost];
[Costs, SortOrder]=sort(Costs);
Solutions=Solutions(SortOrder);
%% ==============Plot/Befor Genetic====================
h=plot(G);
highlight(h,G,'EdgeColor',[0.211 0.2470 0.2410],'LineWidth',0.05,'LineStyle','-');
highlight(h,G,'NodeColor','k','MarkerSize',4);
highlight(h,Solutions(1).Value,'NodeColor','r','MarkerSize',10);
highlight(h,Solutions(1).Value','EdgeColor','r','LineWidth',4);
Title=strcat('Fitness Before Genetic(Red) = ',num2str(Solutions(1).Cost));

%% =============Generations Loop================
tic
for i=1:Generations %Loop 1 to Stop Condition / Generations Step
   %Desort
   %SortOrder=1:popSize;
   Solutions=Solutions(SortOrder);
    if(strcmp(SutvivorSelection,'Generational')==1)
        
        TempSolutions=Solutions;%Initialize Temp OF Solutions
        %-----------Selection Parent / CrossOver------------------
        for c=1:2:nc %CrossOver 
        if (strcmp(func2str(ParentSelection),'Selection_SUS')==1)
            [Parent1,Parent2]=ParentSelection(TempSolutions,Proximity_Matrix,CommiteeSize,Diameter);
            [TempChild1,TempChild2]=CrossOver(TempSolutions(Parent1).Value,TempSolutions(Parent2).Value);
            TempSolutions([Parent1,Parent2])=[];
            %--------------Add Childs
            Child(c).Value=TempChild1;
            Child(c).Cost=FitnessFunction(Proximity_Matrix,TempChild1,CommiteeSize,Diameter);
            Child(c+1).Value=TempChild2;
            Child(c+1).Cost=FitnessFunction(Proximity_Matrix,TempChild2,CommiteeSize,Diameter);
         else
            Parent1=ParentSelection(TempSolutions,Proximity_Matrix,CommiteeSize,Diameter);
            while(1)
                Parent2=ParentSelection(TempSolutions,Proximity_Matrix,CommiteeSize,Diameter);
                if(Parent2~=Parent1)
                    break;
                end
            end
            [TempChild1,TempChild2]=CrossOver(TempSolutions(Parent1).Value,TempSolutions(Parent2).Value);
             TempSolutions([Parent1,Parent2])=[];
           %--------------Add Childs
            Child(c).Value=TempChild1;
            Child(c).Cost=FitnessFunction(Proximity_Matrix,TempChild1,CommiteeSize,Diameter);
            Child(c+1).Value=TempChild2;
            Child(c+1).Cost=FitnessFunction(Proximity_Matrix,TempChild2,CommiteeSize,Diameter);
        end
        end
        %----------------Mutation--------------
        for mloop=1:nm
            SelectedO=randi([1 nc]);
            if(isempty(Child(SelectedO).Value)~=1)
                if (strcmp(func2str(Mutation),'Mutate_Insert')==1)
                Child(SelectedO).Value=Mutation(Child(SelectedO).Value,Size);
                Child(SelectedO).Cost=FitnessFunction(Proximity_Matrix,Child(SelectedO).Value,CommiteeSize,Diameter);
                else
                Child(SelectedO).Value=Mutation(Child(SelectedO).Value); 
                Child(SelectedO).Cost=FitnessFunction(Proximity_Matrix,Child(SelectedO).Value,CommiteeSize,Diameter);
                end
            end
        end
        %-------------Replace Offsprings-----------
         % Sort Population
         Costs=[Solutions.Cost];
         [Costs, SortOrder]=sort(Costs);
         Solutions=Solutions(SortOrder);
        for r=1:nc
        Solutions(r).Value=Child(r).Value;
        Solutions(r).Cost=Child(r).Cost;
        end
    end
    
    if(strcmp(SutvivorSelection,'Steady')==1)
        %Find Parents / Create Childs
        if (strcmp(func2str(ParentSelection),'Selection_SUS')==1)
            [Parent1,Parent2]=ParentSelection(Solutions,Proximity_Matrix,CommiteeSize,Diameter);
            [Child(1).Value,Child(2).Value]=CrossOver(Solutions(Parent1).Value,Solutions(Parent2).Value);
        else
            Parent1=ParentSelection(Solutions,Proximity_Matrix,CommiteeSize,Diameter);
            while(1)
                Parent2=ParentSelection(Solutions,Proximity_Matrix,CommiteeSize,Diameter);
                if(Parent2~=Parent1)
                    break;
                end
            end
            [Child(1).Value,Child(2).Value]=CrossOver(Solutions(Parent1).Value,Solutions(Parent2).Value);
        end
        %Mutation
        for mloop=1:nm
            SelectedO=randi([1 nc]);
            if(isempty(Child(SelectedO).Value)~=1)
                if (strcmp(func2str(Mutation),'Mutate_Insert')==1)
                Child(SelectedO).Value=Mutation(Child(SelectedO).Value,Size);
                else
                Child(SelectedO).Value=Mutation(Child(SelectedO).Value); 
                end
            end
        end
        %Replace Offsprings
         % Sort Population
         Costs=[Solutions.Cost];
         [Costs, SortOrder]=sort(Costs);
         Solutions=Solutions(SortOrder);
        Child(1).Cost=FitnessFunction(Proximity_Matrix,Child(1).Value,CommiteeSize,Diameter);
        Child(2).Cost=FitnessFunction(Proximity_Matrix,Child(2).Value,CommiteeSize,Diameter);
        Solutions(1).Value=Child(1).Value;
        Solutions(2).Value=Child(2).Value;
        Solutions(1).Cost=Child(1).Cost;
        Solutions(2).Cost=Child(2).Cost;
    end
    
    % Sort Population
    Costs=[Solutions.Cost];
    [Costs, SortOrder]=sort(Costs);
    Solutions=Solutions(SortOrder);
    % Store Best Solution
    BestSolution=Solutions(popSize).Value;
    % Store Best Cost
    BestCost=Solutions(popSize).Cost;
    
    for dispi=1:popSize % Show Iteration Information
    disp(['Iteration ' num2str(i) '=' 'Fitness Of Solution ' num2str(dispi) '='  num2str(Solutions(dispi).Cost)] );
    end
end
Time=toc;
%% ==============Plot/After Genetic====================
highlight(h,BestSolution,'NodeColor','g','MarkerSize',10);
highlight(h,BestSolution','EdgeColor','g','LineWidth',4);
Title=[Title,strcat(' | Fitness After Genetic(Green) = ',num2str(BestCost),' | Duration (Time) = ',num2str(Time))];
title(Title);
