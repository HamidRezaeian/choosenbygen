function Commitee=chromosome_Creator(Size,CommiteeSize)
%Size=50;
%CommiteeSize=3;
%num=round(Size/CommiteeSize); %Number Of Parents
%Solutions={};
%for i1=1:num
Commitee_Vector=zeros(Size,1);
Commitee=zeros(CommiteeSize,1);

n=1;
for i=1:CommiteeSize
    while(1)
    temp=randi(Size);
    if (any(Commitee==temp)==0)
        break;
    end
    end

    Commitee_Vector(temp)=1;
    Commitee(n)=temp;
    n=n+1;
end
%Solutions=[Solutions,Commitee];
%end
end