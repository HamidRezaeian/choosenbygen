function [i1,i2]=Selection_SUS(Solutions,Proximity_Matrix,Commitee_Size,Diameter)
    %Evaluate Fitnesses
%     for i=1:length(Solutions)
%         Solutions_Fitness(i)=Fitness(Proximity_Matrix,Solutions(i).Value,Commitee_Size,Diameter);
%     end
    %% 
    %Solutions_Fitness=[0.09 0.001 0.18 0.5];
    Solutions_Fitness =cell2mat({Solutions(:).Cost});
    Sum=1; %If Normilized
    N=length(Solutions_Fitness);
    Pointers=2; 
    PointerDistance=Sum/N;
    FirstRand=rand(Sum)/N;
    
    Solutions_Fitness=Solutions_Fitness./sum(Solutions_Fitness);%Normalizing
    c=cumsum(Solutions_Fitness);%Cumsum
    i1=find(FirstRand<=c,1,'first');
    while(1)
    i2=find(FirstRand+PointerDistance<=c,1,'first');
    FirstRand=FirstRand+PointerDistance;
    if (i2~=i1)
        break;
    end
    end
%     i1=Solutions(i1).Value;
%     i2=Solutions(i2).Value;
end