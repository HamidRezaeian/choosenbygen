function i=RouletteWheel(Solutions_Fitness)
    %Solutions_Fitness=[0.25 0.25 0.25 0.5];
    Solutions_Fitness=Solutions_Fitness./sum(Solutions_Fitness);%Normalizing
    c=cumsum(Solutions_Fitness);%Cumsum
    i=find(rand<=c,1,'first');
end