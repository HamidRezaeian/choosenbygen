function f = Fitness(Proximity_Matrix,chromosome,Commitee_Size,Diameter)
s=chromosome';
%n=1;
%for i=1:length(Commitee_Vector)
    %if (Commitee_Vector(i,1)==1)
        %s(n)=i;
        %n=n+1;
    %end
%end
%% =====================Sum & find m =======================
sum1=0;
m=Proximity_Matrix(s(1),s(2)); %minimum Distance
for i=1:length(s)
    for i1=i+1:length(s)
        if (Proximity_Matrix(s(i),s(i1))<m)
            m=Proximity_Matrix(s(i),s(i1)); %Find m
        end
        sum1=sum1+Proximity_Matrix(s(i),s(i1));
    end
end
%% =====================/k======================
divk=sum1/Commitee_Size;

%% ==================== +m =====================
TopF=divk+m;

%% ================== / 2*D ====================
DownF=TopF/(2*Diameter);

%% =================Return Function ============
f=DownF;

end

