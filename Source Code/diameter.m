function diam = diameter(adj)

diam=0;
for i=1:size(adj,1)
    d=simpleDijkstra(adj,i);
    diam = max([max(d),diam]);
end