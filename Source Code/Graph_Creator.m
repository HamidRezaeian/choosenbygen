function [Graph,Size,Diameter,Proximity_Matrix]=Graph_Creator(GraphSize,FileName)
if (GraphSize~=0)
%GraphSize=40; %Node
RandomGraph = round(rand(GraphSize));
RandomGraph = triu(RandomGraph) + triu(RandomGraph,1)';
RandomGraph = RandomGraph - diag(diag(RandomGraph));
Graph=graph(RandomGraph);
else
A =UniqueG(ReadGML(FileName));
Graph = graph(A(:,1), A(:,2));
end
Size=numnodes(Graph);
Diameter=diameter(Graph.adjacency);
Proximity_Matrix=distances(Graph);
end