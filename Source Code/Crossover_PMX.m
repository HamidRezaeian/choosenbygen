function [Child1Return,Child2Return]=Crossover_PMX(Parent1,Parent2)
%Parent1=[1;2;3;4;5;6];
%Parent2=[6;5;4;3;2;1];
Child1=Parent1';
Child2=Parent2';
%% ================Split===================
start=randi([2,length(Parent1)-1]);
ending=randi([start,length(Parent1)-1]);
%% ========Swap Splited & Create RelationMap===========
%Cut
temp1=Child1(start:ending);
temp2=Child2(start:ending);
%swap
Child1=[[Child1(1:start-1),temp2],Child1(ending+1:length(Parent1))];
Child2=[[Child2(1:start-1),temp1],Child2(ending+1:length(Parent2))];
%relation map
for i=1:length(temp1)
Relation(1,i)=temp1(i);
Relation(2,i)=temp2(i);

    %find Same Relations Part
    for i1=1:i
        if (Relation(1,i1)==Relation(1,i))||(Relation(2,i1)==Relation(1,i))
            Include=Relation(1,i);
            IncludeNum=i1;
        end
        if (Relation(1,i1)==Relation(2,i))||(Relation(2,i1)==Relation(2,i))
            Include=Relation(2,i);
            IncludeNum=i1;
        end
    end
%Swap_relation map
Childe1xclude=[Child1(1:start-1),Child1(ending+1:length(Parent1))];
Childe2xclude=[Child2(1:start-1),Child2(ending+1:length(Parent2))];
    for i1=1:length(Childe1xclude)
        if (IncludeNum==i)
        if (Relation(1,i)==Childe1xclude(i1) || Include==Childe1xclude(i1))
            for i2=1:length(Childe2xclude)
                if (Relation(2,i)==Childe2xclude(i2)|| Include==Childe2xclude(i2))
                   SwapTemp1=Childe1xclude(i1);
                   SwapTemp2=Childe2xclude(i2);
                   %swap
                   Childe1xclude(i1)=SwapTemp2;
                   Childe2xclude(i2)=SwapTemp1;
                end
            end
        end
        if (Relation(2,i)==Childe1xclude(i1) || Include==Childe1xclude(i1))
        for i2=1:length(Childe2xclude)
            if (Relation(1,i)==Childe2xclude(i2)|| Include==Childe2xclude(i2))
               SwapTemp1=Childe1xclude(i1);
               SwapTemp2=Childe2xclude(i2);
               %swap
               Childe1xclude(i1)=SwapTemp2;
               Childe2xclude(i2)=SwapTemp1;
            end
        end
        end
        else if (Relation(1,i)==Childe1xclude(i1))
            
               for i2=1:length(Childe2xclude)
                if (Relation(2,i)==Childe2xclude(i2))
                   SwapTemp1=Childe1xclude(i1);
                   SwapTemp2=Childe2xclude(i2);
                   %swap
                   Childe1xclude(i1)=SwapTemp2;
                   Childe2xclude(i2)=SwapTemp1;
                end
                end
            end
            if (Relation(2,i)==Childe1xclude(i1) || Include==Childe1xclude(i1))
            for i2=1:length(Childe2xclude)
                if (Relation(1,i)==Childe2xclude(i2)|| Include==Childe2xclude(i2))
                   SwapTemp1=Childe1xclude(i1);
                   SwapTemp2=Childe2xclude(i2);
                   %swap
                   Childe1xclude(i1)=SwapTemp2;
                   Childe2xclude(i2)=SwapTemp1;
                end
        end
        end
        end
    end
% Rebuild Childs
Child1=[[Childe1xclude(1:start-1),temp2],Childe1xclude((ending-length(temp1))+1:length(Childe1xclude))];
Child2=[[Childe2xclude(1:start-1),temp1],Childe2xclude((ending-length(temp1))+1:length(Childe2xclude))];
Child1Return=Child1';
Child2Return=Child2';
end
end