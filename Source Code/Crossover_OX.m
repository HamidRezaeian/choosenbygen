function [Child1,Child2]=Crossover_OX(Parent1,Parent2)
%Parent1=[1;2;3;4;5;6];
%Parent2=[6;5;4;3;2;1];
Child1=Parent1';
Child2=Parent2';
%% ================Split===================
start=randi([2,length(Parent1)-1]);
ending=randi([start,length(Parent1)-1]);
%% ========Partition & Swap===========
%Split_Part
Split1=Child1(start:ending);
Split2=Child2(start:ending);
%First_Part
First1=Child1(1:start-1);
First2=Child2(1:start-1);
%End_Part
End1=Child1(ending+1:length(Parent1));
End2=Child2(ending+1:length(Parent2));
%OX_Sort
Sort1=[End1,First1,Split1];
Sort2=[End2,First2,Split2];
%% ===============Swap====================
%Swap_Ending_Child1
SortCount2=1;
for i=1:length(End1)
    for i1=SortCount2:length(Sort2)
     if (any(Sort2(i1)==Split1)==0)
        End1(i)= Sort2(i1);
        SortCount2=SortCount2+1;
        break;
     else
        SortCount2=SortCount2+1;
     end
    end
end
%Swap_First_Child1
for i=1:length(First1)
    for i1=SortCount2:length(Sort2)
     if (any(Sort2(i1)==Split1)==0)
        First1(i)= Sort2(i1);
        SortCount2=SortCount2+1;
        break;
     else
        SortCount2=SortCount2+1;
     end
    end
end
%Swap_Ending_Child2
SortCount1=1;
for i=1:length(End2)
    for i1=SortCount1:length(Sort1)
     if (any(Sort1(i1)==Split2)==0)
        End2(i)= Sort1(i1);
        SortCount1=SortCount1+1;
        break;
     else
        SortCount1=SortCount1+1;
     end
    end
end
%Swap_First_Child2
for i=1:length(First2)
    for i1=SortCount1:length(Sort1)
     if (any(Sort1(i1)==Split2)==0)
        First2(i)= Sort1(i1);
        SortCount1=SortCount1+1;
        break;
     else
        SortCount1=SortCount1+1;
     end
    end
end
%% ================Rebuild Childs===============
Child1=[[First1,Split1],End1]';
Child2=[[First2,Split2],End2]';
end