function [A] = UniqueG(inp)
inp=unique(inp,'rows');
d=1;
while (d < length(inp))
    if inp(d,1)*inp(d,2)==0
       inp=removerows(inp,d); 
       d=d-1;
    end
     d=d+1;
end

flipinp=fliplr(inp);
d=1;
while (d < length(inp))
    d1=1;
    while (d1 < length(flipinp))
        if d==0
            d=1;
        end
        if d1==0
            d1=1;
        end
        if inp(d,:)==flipinp(d1,:)
        inp=removerows(inp,d);
        flipinp=removerows(flipinp,d1);
        d1=d1-1;
        d=d-1;
        end
    d1=d1+1;
    end
    d=d+1;
end

A=inp;
end