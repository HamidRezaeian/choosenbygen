function i=Selection_Tournament(Solutions,Proximity_Matrix,Commitee_Size,Diameter)
    %Evaluate Fitnesses
%     for i=1:length(Solutions)
%         Solutions_Fitness(i)=Fitness(Proximity_Matrix,Solutions(i).Value,Commitee_Size,Diameter);
%     end
    %% 
    %Solutions_Fitness=[0.09 0.001 0.18 0.5];
    Solutions_Fitness =cell2mat({Solutions(:).Cost});
    K=randi([2,length(Solutions_Fitness)]);
    MaxFitness=0;
    for h=1:K
        P=randi(length(Solutions_Fitness));
        if (Solutions_Fitness(P)>MaxFitness)
            MaxFitness=Solutions_Fitness(P);
            i=P;
        end
    end
%     i=Solutions(i).Value;
end