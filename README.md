# DSS Project

![logo](markdown/G3.png) ![logo](markdown/G1.png)

This is my project for ‘decision support  systems’ course at pasargad university. I have done this project individually in two weeks with Matlab. Also this work is inspired by Eduardo Zamudio's paper with the title: "Social networks and genetic algorithms to choose committees with independent members".
